#include "gdfileio.h"

/**
 * @brief Contructor for the FileIO lib. This is not used in Godot 3.0+ _init
 * takes its place
 */
godot::FileIO::FileIO()
{
}

/**
 * @brief Deconstructor, called when cleaning up post library close.
 */
godot::FileIO::~FileIO()
{
	close_file();
}

/**
 * @brief The actual init code used by Godot 3.0
 */
void godot::FileIO::_init()
{
}

/**
 * @brief Registering methods with Godot. I.e dictating what methods are
 * callable directly through GDNative/GDScript
 */
void godot::FileIO::_register_methods()
{
	register_method("NEXT_FILE_CHUNK", &FileIO::next_file_chunk);
	register_method("PREV_FILE_CHUNK", &FileIO::prev_file_chunk);
	register_method("SAVE_CHUNK", &FileIO::save_chunk);
	register_method("OPEN_FILE", &FileIO::open_file);
	register_method("CLOSE_FILE", &FileIO::close_file);
}

/**
 * @brief Load a file pointer into memory and read the first chunk of the file
 * of size BUFFER_SIZE into the read_buffer. Logging the starting pointer and
 * ending pointer into the buffer_pointers map.
 * @param godot_file_path path of the file we wish to open
 * @return The read_buffer data converted to a string.
 */
godot::String godot::FileIO::open_file(godot::String godot_file_path)
{
	current_open_filename = godot_file_path.alloc_c_string();
	file_reader.open(current_open_filename, std::ios::in | std::ios::out);
	if (file_reader.is_open()) {
		file_reader.read(read_buffer.get(), BUFFER_SIZE);
		buffer_pointers[buffer_count] =
			make_pair(0, file_reader.tellg());
		return read_buffer.get();
	}
	return "";
}

/**
 * @brief Load the next chunk of the file of size BUFFER_SIZE into the
 * read_buffer, and log the starting and ending points of the new chunk into the
 * buffer_pointers map.
 * @return The read_buffer data converted to a string.
 */
godot::String godot::FileIO::next_file_chunk()
{
	if (file_reader.is_open() && file_reader.peek() != EOF) {
		std::streamoff starting_point = file_reader.tellg();
		file_reader.read(read_buffer.get(), BUFFER_SIZE);
		buffer_pointers[++buffer_count] =
			make_pair(starting_point, file_reader.tellg());
		return read_buffer.get();
	}
	return "";
}

/**
 * @brief Fetch the previous chunk of the file based on the starting pointer
 * saved in buffer_pointers.
 * @return The read_buffer data converted to a string.
 */
godot::String godot::FileIO::prev_file_chunk()
{
	if (file_reader.is_open()) {
		if (buffer_count > 0) {
			file_chunk prev_chunk = buffer_pointers[--buffer_count];
			file_reader.seekg(prev_chunk.first);
			file_reader.read(read_buffer.get(), BUFFER_SIZE);
			return read_buffer.get();
		}
	}
	return "";
}

/**
 * @brief Save the modified chunk into a file. If the path is not the same as
 * the current open file make a copy of the file and replace the chunk that has
 * been edited with the new one.
 * @param path location where we want to save the modified file to.
 * @param modified_chunk modified chunk which we want to use to overwrite our
 * current chunk in memory.
 */
void godot::FileIO::save_chunk(godot::String path, godot::String modified_chunk)
{
	if (file_reader.is_open()) {
		std::string filename = path.alloc_c_string();
		file_chunk curr_chunk = buffer_pointers[buffer_count];
		if (filename != current_open_filename) {
			make_new_file(filename);
		}
		// Just saving the chunk inplace
		file_reader.seekp(curr_chunk.first);
		file_reader << modified_chunk.alloc_c_string();
		buffer_pointers[buffer_count].second = file_reader.tellp();
	}
}

/**
 * @brief Close the file pointer to the currently opened file
 */
void godot::FileIO::close_file()
{
	if (file_reader.is_open()) {
		file_reader.close();
	}
}

/**
 * @brief Create a new file in memory and copy the current open file content to
 * it.
 * @param new_filename New file we want to create and copy data to.
 */
void godot::make_new_file(std::string new_filename)
{
	// Saving chunk to new file
	std::fstream new_reader;
	new_reader.open(current_open_filename, std::ios::in | std::ios::out);
	if (new_reader.is_open()) {
		file_reader.seekg(0);
		new_reader << file_reader.rdbuf();
	}
	close_file(); // Close old file
	file_reader = new_reader; // Keep track of only new file
}
