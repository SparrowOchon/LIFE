//
// Created by null on 1/3/22.
//

#ifndef SRC_FileIO_H
#define SRC_FileIO_H

#include <Godot.hpp>
#include <String.hpp>
#include <Reference.hpp>
#include <cstdio>
#include <fstream>
#include <unordered_map>
#include <memory>
#include <string>
#include <sstream>

namespace godot
{
class FileIO : public Reference {
	GODOT_CLASS(FileIO, Reference);

    private:
	/**
	* @brief A typedef for the pair that stores the start and endpoint of the file buffer read.
	*/
	typedef std::pair<std::streamoff, std::streamoff> file_chunk;

	/**
	* @brief Define the size of data to load into memory when reading I.e chunk size.
	*/
	static constexpr size_t BUFFER_SIZE = 1024 * 512; // 2 MB Chunks

	/**
	* @brief The active file stream to keep the file open while we read/write chunks to it.
	*/
	std::fstream file_reader;

	/**
	* @brief The buffer to store the active file chunk.
	*/
	std::unique_ptr<char[]> read_buffer{ new char[BUFFER_SIZE] };

	/**
	* @brief The map that stores the chunk number, as well as the pair of starting and ending point for the chunk.
	*/
	std::unordered_map<size_t, file_chunk> buffer_pointers;

	/**
	* @brief Track which start and end point we are at in the buffer
	*/
	size_t buffer_count = 0;

	/**
	* @brief Store current open filename, needed to validate when saving.
	*/
	godot::String current_open_filename;

	/**
	* @brief Create a new file in memory and copy the current open file content to it.
	* @param new_filename New file we want to create and copy data to.
	*/
	void make_new_file(std::string new_filename);

    public:
	static void _register_methods();

	FileIO();
	~FileIO();

	void _init(); // our initializer called by Godot
	godot::String open_file(godot::String file_path);
	godot::String next_file_chunk();
	godot::String prev_file_chunk();
	void save_chunk(godot::String file_path, godot::String modified_chunk);
	void close_file();
};
} // namespace godot

#endif // SRC_FileIO_H
