# Large Independent File Editor (LIFE)

The following is a POC of a file editor with the ability to read files of sizes of over 200GB+ with a hard limitation of being under 2GB of RAM usage.
The editor uses a "chunking" approach I.e, by opening a file pointer and then only loading a predefined amount of bytes into memory per
pull. Effectively allowing you to efficiently open and edit any file you have stored on your Disk regardless of the amount of RAM or CPU.

![rock_you_img](image/rockyou_test.png)

## Features

- Easily modifiable Godot Frontend
- Independent C++ filehandling module.
- Syntax Highlighting
- Ability to Open/Edit/Save.
- Cross-platform compatible (Mac, Windows, Linux, and Android)

## Missing Features

As this is made as a POC, the following was not added, but all can be easily added through the C++ Library:

- Threaded Find/Search/Replace between the chunks (Works for active chunk only)
- Restore previously opened files
- Saving all previous chunks. I.e, if a new chunk is loaded, the file will discard all changes unless saved beforehand.
- Doesn't set the pagefile to the chunksize on non POSIX platforms.

## How it Works

- `ui/app.gd` : The main codebase used to both Manage UI and call Library.
- `gdnative/src/gdfileio.cpp`: FileLibrary responsible for managing and loading chunks.

## Building Library

- Open GDNative and build `scons platform=<platform> target=release` replace `<platform>` with `windows`, `osx` or `linux`

## Special Thanks

- [Mounir Tohami for the Theme](https://mounirtohami.itch.io/godot-dark-theme)

## License

The following codebase is licensed entirely under [Share don't Sell License](https://gitlab.com/SparrowOchon/show-dont-sell)
. PLEASE READ CAREFULLY.
