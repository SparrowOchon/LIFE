extends Control

enum FileMenuOptions { NEWFILE, OPENFILE, SAVEAS, SAVE, QUIT }

enum HelpMenuOptions { ABOUT }

enum ViewMenuOptions { MINIMAP }

const APP_NAME = "Text Editor" # Default program name
const NEW_FILE_NAME = "Untitled" # Default file name

var current_file = NEW_FILE_NAME # Current active open file
var last_line = 0 # Last line of open file
var file_io = load("res://include/gdfileio.gdns").new() # Load the C++ Library into GDScript

func _ready():
##	
##	Called when the node enters the scene tree for the first time.
##	
	update_opened_file(NEW_FILE_NAME)

	make_shortcut(FileMenuOptions.SAVE, KEY_S)
	make_shortcut(FileMenuOptions.QUIT, KEY_Q)

	$MenuBar/FileMenuButton.get_popup().connect("id_pressed", self, "_on_FileMenu_item_pressed")
	$MenuBar/HelpMenuButton.get_popup().connect("id_pressed", self, "_on_HelpMenu_item_pressed")
	$MenuBar/ViewMenuButton.get_popup().connect("id_pressed", self, "_on_ViewMenu_item_pressed")


func _input(event):
##
##	Capture the mouse scroll input in godot, if at end of file load next chunk if at the top load prev chunk
##
##	:param event: any event triggered in godot (Trigger, button, scroll, etc)
##
	if event is InputEventMouseButton && last_line != 0:
		var cursor_caret_pos = $TextArea.cursor_get_line()
		if event.button_index == BUTTON_WHEEL_UP && cursor_caret_pos == 0:
			chunk_read(0)
		if event.button_index == BUTTON_WHEEL_DOWN && cursor_caret_pos == last_line:
			chunk_read(1)


func chunk_read(type):
##
##	Get the next/prev chunk from the File and update the TextArea with the new text
##
##	:param type: Identify which chunk to fetch 0: prev chunk, otherwise fetch next chunk.
##
	var chunk = ""
	if type == 0:
		chunk = file_io.PREV_FILE_CHUNK()
	else:
		chunk = file_io.NEXT_FILE_CHUNK()
	if !chunk.empty():
		$TextArea.text = $TextArea.get_line(last_line) + chunk
		last_line = $TextArea.get_line_count() - 1  # Doesnt start at 0


func close_active_file():
##
##	Close the Openfile
##
	if current_file != NEW_FILE_NAME:
		file_io.CLOSE_FILE()


func write_to_file(path):
##
##	Save modified textarea into a new updated textfile at location path.
##		NOTE: This will make a copy of the file if the path is different from the original one. (SAVE AS)
##
##	:param path: Location to save modified file to. If path is the same as the opened file it will simply update it inplace.
##		Otherwise it will create a new file.
##	
	file_io.SAVE_CHUNK(path, $TextArea.text)


func read_from_file(path):
##
##	Open a new file at location path and fetch its first chunk into the textarea
##
##	:param path: Location of file to read.
##
	$TextArea.text = file_io.OPEN_FILE(path)
	last_line = $TextArea.get_line_count() - 1  # Doesnt start at 0


#--------------------------------------------------------------------------------------------------
#											MENU BAR ITEMS
#
#--------------------------------------------------------------------------------------------------


func _on_ViewMenu_item_pressed(id):
##
##	 Event Listener for View button in MenuBar
##
##	 :param id: The id of the item in the View dropdown list
##
	match id:
		ViewMenuOptions.MINIMAP:
			$TextArea.minimap_draw = !$TextArea.minimap_draw


func _on_FileMenu_item_pressed(id):
##
##	 Event Listener for File button in MenuBar
##
##	 :param id: The id of the item in the File dropdown list
##
	match id:
		FileMenuOptions.NEWFILE:
			close_active_file()
			update_opened_file(NEW_FILE_NAME)
			$TextArea.text = ""
		FileMenuOptions.OPENFILE:
			close_active_file()
			$OpenFileDialog.popup()
		FileMenuOptions.SAVE:
			if current_file == NEW_FILE_NAME:
				$SaveFileDialog.popup()
			else:
				write_to_file(current_file)
		FileMenuOptions.SAVEAS:
			$SaveFileDialog.popup()
		FileMenuOptions.QUIT:
			close_active_file()
			get_tree().quit()


func _on_HelpMenu_item_pressed(id):
##
##	 Event Listener for Help button in MenuBar
##
##	 :param id: The id of the item in the Help dropdown list
##
	match id:
		HelpMenuOptions.ABOUT:
			$AboutWindowDialog.popup()


#
#--------------------------------------------------------------------------------------------------
#												FILE DIALOGS
#
#--------------------------------------------------------------------------------------------------


func _on_OpenFileDialog_file_selected(path):
##
##	 Event Listener for Open in the OpenFile Dialog
##
##	 :param path: Path of file we want to open
##
	read_from_file(path)
	update_opened_file(path)


func _on_SaveFileDialog_file_selected(path):
##
##	 Event Listener for Save in the SaveFile Dialog
##
##	 :param path: Path of file we want to Save
##
	write_to_file(path)
	update_opened_file(path)


#--------------------------------------------------------------------------------------------------
#											GODOT ADDING FUNCTIONALITY
#
#--------------------------------------------------------------------------------------------------
func _notification(what):
##
##	 Event Listener for capturing edits to the MainLoop
##
##	 :param what: Event that happened to the program.
##
	# Handle PROGRAM EXIT
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		close_active_file()
		get_tree().quit()  # default behavior


func make_shortcut(item_id, activation_key):
##
##	 Make a Control based Shortcut
##
##	 :param item_id: The FileMenu ID we want to add the shortcut to
##	 :param activation_key: The Key we want to use with Ctrl to trigger the item_id function.
##
	var shortcut = ShortCut.new()
	var input_event_key = InputEventKey.new()
	input_event_key.set_scancode(activation_key)
	input_event_key.control = true
	shortcut.set_shortcut(input_event_key)
	$MenuBar/FileMenuButton.get_popup().set_item_shortcut(item_id, shortcut, true)


func update_opened_file(working_path):
##
##	 Update the Program Title
##
##	 :param working_path: Path of file we have open
##
	OS.set_window_title(APP_NAME + " - " + working_path)
	current_file = working_path


func _on_GitlabButton_button_down():
	OS.shell_open("https://gitlab.com/SparrowOchon")


func _on_LicenseButton_button_down():
	OS.shell_open("https://gitlab.com/SparrowOchon/show-dont-sell/-/raw/master/LICENSE.md")
